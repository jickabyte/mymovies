<?php

return array(
    'backoffice' => array(
        'className' => 'Backoffice\Module',
        'path' => '../app/modules/backoffice/Module.php',
    ),
    'frontend' => array(
        'className' => 'Frontend\Module',
        'path' => '../app/modules/frontend/Module.php',
    )

);