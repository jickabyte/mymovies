<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Config\Adapter\Ini as ConfigIni;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Mvc\Router;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


$di = new FactoryDefault();
$di->set('dispatcher', function () use ($di) {

    $eventsManager = new EventsManager();
    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

/*
 * Routing 
 */
$di->set(
    'router',
    function () {
        $routes = require(APP_PATH . 'app/config/routes.php');
        $router = new Router();
        foreach ($routes as $r) {
            $router->add($r['uri'], $r['route']);
        }

        return $router;
    }
);

/*
 * Config
 */
$di->set('config', function () {
    $config = new ConfigIni(APP_PATH . 'app/config/config-' . SERVER_MODE . '.ini');

    return $config;
});

/*
 * Database
 */
$di->set('db', function () use ($di) {
    return new Database(array(
        "host" => $di->get("config")->database->host,
        "username" => $di->get("config")->database->username,
        "password" => $di->get("config")->database->password,
        "dbname" => $di->get("config")->database->dbname,
        "charset" => $di->get("config")->database->charset
    ));
});


