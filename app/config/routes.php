<?php

return array(
    array(
        'uri' => '/:module/:controller/:action/:params',
        'route' => array(
            'module' => 1,
            'controller' => 2,
            'action' => 3,
            'params' => 4,
        )
    ),
    array(
        'uri' => '/frontend',
        'route' => array(
            'module' => 'frontend',
            'controller' => 'app',
            'action' => 'index',
        )
    ),
    array(
        'uri' => '/backoffice',
        'route' => array(
            'module' => 'backoffice',
            'controller' => 'app',
            'action' => 'index',
        )
    )
);