<?php
namespace Backoffice\Controllers;

use Phalcon\Mvc\Controller;
use Backoffice\Models\Users;

class ControllerBase extends Controller
{
    public function initialize()
    {
       
        $this->view->titlepage = 'My Movies Back Office';
        $this->view->server_mode = SERVER_MODE;
        
    }
}
?>