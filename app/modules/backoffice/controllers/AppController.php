<?php
namespace Backoffice\Controllers;

use Phalcon\Http\Response;
use Backoffice\Models\Movies;

class AppController extends ControllerBase
{
    public function initialize(){
        parent::initialize();
    }
    
    public function IndexAction(){
        $movies = Movies::find();  
        
        if(count($movies) > 0){
            $res = json_encode($movies, JSON_PRETTY_PRINT);
            echo "<pre>".($res);
            exit();
        }
    }
}


?>