<?php

namespace Backoffice;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\DiInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;

class Module implements ModuleDefinitionInterface
{
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces(
            array(
                'Backoffice\Controllers' => '../app/modules/backoffice/controllers/',
                'Backoffice\Models'      => '../app/modules/backoffice/models/',
            )
        );

        $loader->register();

    }

    /**
     * Register specific services for the module
     */
    public function registerServices(DiInterface $di)
    {
        // Registering a dispatcher
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('Backoffice\Controllers');
            return $dispatcher;
        });

        /**
         * Registering the view component
         * Setting up volt
         * Setting cache volt
         */
        $di->set('view', function () {
            $view = new View();
            $view->setViewsDir('../app/modules/backoffice/views/');

            $view->registerEngines([
                '.volt' => function ($view, $di) {
                    // Volt Template Engine
                    $volt = new VoltEngine($view, $di);

                    $volt->setOptions(array(
                        "compiledPath" => APP_PATH . "cache/volt/",
                        'compileAlways'     => true 
                    ));
                    $compiler = $volt->getCompiler();
                    $compiler->addFunction('is_a', 'is_a');

                    return $volt;
                },
            ]);

            return $view;
        }, true);

        $di->set('connectdb', function() use ($di) {
            return new Database(array(
                "host" => $di->get("config")->database->host,
                "username" => $di->get("config")->database->username,
                "password" => $di->get("config")->database->password,
                "dbname" => $di->get("config")->database->dbname,
                "charset" => $di->get("config")->database->charset
            ));
        });
    }
}