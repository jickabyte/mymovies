<?php
namespace Backoffice\Models;

use Phalcon\Mvc\Model;

class Movies extends Model
{
    public $id;
    public $name;
    public $director;
    public $release_year;

    public function initialize()
    {
        $this->setSource('movies');

    }

    
}
