{% extends "layouts/base.volt" %}

{% block body %}
<div class="container-fluid">
    <div class="row" style="padding-top:15px">
        <div class="col-sm-12 col-md-12">
            <div class="card border-info">
                <div class="card-body">
                        <ul class="nav nav-pills">
                        <li class="nav-item">
                        <a class="nav-link active" id="browse_tap" href="#">Browse</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" id="add_tap" href="#">Add Movie</a>
                        </li>

                        </ul>
                        </div>
                    
                     <!-- Browse Movie -->
                        <div class="card-body" id="browse_wrapper" >
                                
                                    <!-- Browse -->
                                    <div class="jumbotron" >
                                            <h1 class="display-3">Browse Movie</h1>
                                            <p class="lead">Browse your movie</p>
                                            
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Movie Name</label>
                                                        <input class="form-control" id="b_name" type="text" placeholder="Enter movie name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Director Name</label>
                                                        <input class="form-control" id="b_director" type="text" placeholder="Enter director name">
                                                    </div>
                                                </div>
                                            </div>
                                                   
                                                    
                                            <div class="row">
                                                
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                <label for="year">Release Year</label>
                                                <input class="form-control" id="b_year" type="text" placeholder="1999">
                                                </div>
                                                </div>
                                                </div>
                                                <a class="btn btn-primary btn-md" href="#" role="button" id="browse_submit">Browse</a>
                                        </div>
                                        <!-- Edit -->
                                        <div class="jumbotron" style="padding:1rem 1rem; display: none;" id="edit_wrapper">
                                                
                                                <p class="lead">Edit Movie</p>
                                                
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="name">Movie Name</label>
                                                            <input class="form-control" id="edit_name" type="text" placeholder="Enter movie name">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="name">Director Name</label>
                                                            <input class="form-control" id="edit_director" type="text" placeholder="Enter director name">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label for="year">Release Year</label>
                                                            <input class="form-control" id="edit_year" type="text" placeholder="1999">
                                                            <input type="hidden" id="hid_id_edit" >
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                    <a class="btn btn-primary btn-md" href="#" role="button" id="edit_btn_submit">Edit</a>
                                            </div>
                                        <!-- End Edit -->
                            <!-- Table -->
                            {% if movie_list %}
                            <table class="table table-responsive-sm table-striped" id="movie_table">
                                <thead>
                                    <tr>
                                        <th>Movie name</th>
                                        <th>Director</th>
                                        <th>Release Year</th>
                                        <th>Tool</th>
                                    </tr>
                                </thead>
                                <tbody id="table_body">
                                        {% for list in movie_list %}
                                            <tr>
                                                <td id="movie_name_{{list.id}}">{{ list.name }}</td>
                                                <td id="movie_director_{{list.id}}">{{ list.director }}</td>
                                                <td id="movie_year_{{list.id}}">{{ list.release_year }}</td>
                                                <td>
                                                    <button class="btn btn -brand btn-facebook" type="button" style="margin-bottom: 4px" onClick="edit_btn( '{{list.id}}' )">
                                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </button>
                                                    <button class="btn btn -brand btn-youtube delete_btn" type="button" style="margin-bottom: 4px" onClick="delete_btn( '{{list.id}}' )">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        {% endfor %}
                                </tbody>
                            </table>
                           
                            {% else %}
                                {{ err }}
                            {% endif %}
                    </div>
                    <!-- Start Add Movie -->
                    <div class="card-body" id="add_movie_wrapper" style="display:none;">
                        <div class="jumbotron" >
                            <h1 class="display-3">Add Movie</h1>
                            <p class="lead">Add your movie</p>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Movie Name</label>
                                        <input class="form-control" id="add_movie_name" type="text" placeholder="Enter movie name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Director Name</label>
                                        <input class="form-control" id="add_director" type="text" placeholder="Enter director name">
                                    </div>
                                </div>
                            </div>
                                   
                                    
                            <div class="row">
                                
                                <div class="col-sm-6">
                                <div class="form-group">
                                <label for="year">Release Year</label>
                                <input class="form-control" id="add_year" type="text" placeholder="1999">
                                </div>
                                </div>
                                </div>
                                <a class="btn btn-primary btn-md" href="#" role="button" id="add_movie_btn">Add</a>
                        </div>
                        <!-- End Add movie -->
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {

        $('#add_tap').on('click', function(){
            document.getElementById('browse_wrapper').style.display = 'none';
            document.getElementById('add_movie_wrapper').style.display = 'block';
            
            var browse_tap = document.getElementById('browse_tap');
            var add_tap = document.getElementById('add_tap') ;
            if(!add_tap.classList.contains('active')){
                add_tap.className += ' active';
                browse_tap.className = browse_tap.className.replace('active','')
            
            }
            
        });

        $('#browse_tap').on('click', function(){
            var browse_tap = document.getElementById('browse_tap');
            var add_tap = document.getElementById('add_tap') ;
            if(browse_tap.classList.contains('active')){
                location.reload();
            }else{
                document.getElementById('browse_wrapper').style.display = 'block';
                document.getElementById('add_movie_wrapper').style.display = 'none';
                
                
                if(!browse_tap.classList.contains('active')){
                    browse_tap.className += ' active';
                    add_tap.className = add_tap.className.replace('active','')
                
                }
            }

        });
        
        $('#add_movie_btn').on('click', function(){
            
            var movie_name = document.getElementById('add_movie_name').value;
            var movie_director = document.getElementById('add_director').value ;
            var movie_year = document.getElementById('add_year').value ;

            if(movie_name !='' && movie_director != '' && movie_year != ''){
                
                if(IsNumeric(movie_year)){
                    // Add Movie to database 
                    $.ajax({
                        url: 'frontend/app/addmovie',
                        data: JSON.stringify({"name": movie_name, "director": movie_director, "year": movie_year}),
                        type: "POST",
                        dataType: 'json',
                        success: function(res){
                            alert(res['msg']);
                            location.reload();
                        }
                    });
                }else{
                    alert('Please enter correctly year');
                }
            }else{
                alert('Please fill the form');
            }
            

        });

        function IsNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        //Edit submit
        $('#edit_btn_submit').on('click', function(){
            
            var id = document.getElementById('hid_id_edit').value
            var n = document.getElementById('edit_name').value
            var d = document.getElementById('edit_director').value
            var y = document.getElementById('edit_year').value

            $.ajax({
            url: 'frontend/app/editmovie',
            data: JSON.stringify({
                "id":id,
                "name":n,
                "director":d,
                "year":y}),
            type: "POST",
            dataType: 'json',
            success: function(res){
                alert(res['msg']);
                location.reload();
            }
            });

        });

        //Browse submit
        $('#browse_submit').on('click', function(){
            
            var n = document.getElementById('b_name').value
            var d = document.getElementById('b_director').value
            var y = document.getElementById('b_year').value

            $.ajax({
            url: 'frontend/app/browsemovie',
            data: JSON.stringify({
                "name":n,
                "director":d,
                "year":y}),
            type: "POST",
            dataType: 'json',
            success: function(res){
                alert(res['msg']);
                document.getElementById('table_body').innerHTML = '';
                if(res['success']){
                    if(res['data']){
                        var html = "";
                        $.each(res['data'],function(index, data){
                            html += 
                            '<tr>'+
                                '<td id="movie_name_'+data.id+'">' + data.name + '</td>' +
                                '<td id="movie_director_' + data.id + '">' + data.director +'</td>' +
                                '<td id="movie_year_'+data.id +'">' + data.release_year +'</td>' +
                                '<td>'+
                                    '<button class="btn btn -brand btn-facebook" type="button" style="margin-bottom: 4px" onClick="edit_btn( ' +data.id +' )">' +
                                            '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                                    '</button>' +
                                    '<button class="btn btn -brand btn-youtube delete_btn" type="button" style="margin-bottom: 4px" onClick="delete_btn( '+ data.id+ ' )">'+
                                            '<i class="fa fa-trash" aria-hidden="true"></i>'+
                                    '</button>'+
                                '</td>'+
                            '</tr>';
                        });
                        $('#table_body').append(html);
                    }

                }else{

                }
            }
            });

        });
    });

    
   
    function edit_btn(id){
        // document.getElementById('edit_wrapper').style.display = 'none';
        document.getElementById('edit_wrapper').style.display = 'block';
        document.getElementById('edit_name').focus();

        //get value
        var temp_name = document.getElementById('movie_name_'+id).textContent ;
        var temp_director = document.getElementById('movie_director_'+id).textContent ;
        var temp_year = document.getElementById('movie_year_'+id).textContent ;

        //fill value
        document.getElementById('edit_name').value = temp_name;
        document.getElementById('edit_director').value = temp_director;
        document.getElementById('edit_year').value = temp_year;
        document.getElementById('hid_id_edit').value = id;
 
    }

    function delete_btn(id){
        
        $.ajax({
            url: 'frontend/app/deletemovie',
            data: JSON.stringify({"id":id}),
            type: "POST",
            dataType: 'json',
            success: function(res){
                alert(res['msg']);
                location.reload();
            }
        });
    }



</script>

{% endblock %}