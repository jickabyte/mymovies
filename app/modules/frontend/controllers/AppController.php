<?php
namespace Frontend\Controllers;

use Phalcon\Http\Response;
use Frontend\Models\Movies;

class AppController extends ControllerBase
{
    public function initialize(){
        parent::initialize();
    }
    
    public function IndexAction(){
        $movies = [];
        $movies = Movies::find(array('order' => 'release_year DESC'));  
        if(count($movies) > 0){
            $this->view->movie_list = $movies;
            $this->view->assets_domain = $this->config->get("application")["assetsDomain"];
            
        }else{
            $this->view->err = "No Movie found";
        }
    }

    public function browsemovieAction(){
        $data = $this->request->getJsonRawBody();
        $name = $data->name;
        $director = $data->director;
        $year = $data->year;
        $msg = '';
        $conditions = array();
        $conds = '';
        if(strlen($name)>0){
            array_push($conditions, "`name` LIKE '$name%' ");
        }
        if(strlen($director)>0){
            array_push($conditions, " `director` LIKE '$director%' ");
        }
        if(strlen($year)>0){
            array_push($conditions, " `release_year` LIKE '$year%' ");
        }

        foreach($conditions as $cond){
            $conds = $conds .$cond .'||';
        }
        $conds = substr($conds, 0, -2);
        $sql = "SELECT * FROM `movies` WHERE $conds ORDER BY `release_year` DESC";

        $movies = $this->connectdb->fetchAll($sql);

        if(count($movies)>0){
            $msg = 'Found ' . sizeof($movies) .' movie(s) !';
            echo json_encode(array('success' => true, 'msg'=> $msg , 'data'=> $movies));
            exit();
        }else{
            $msg = 'Movie not found!';
            echo json_encode(array('success' => false, 'msg'=> $msg ));
            exit();
        }

    }

    public function addmovieAction(){
        $data = $this->request->getJsonRawBody();
        $name = $data->name;
        $director = $data->director;
        $year = $data->year;
        $msg = '';

        $movie = new Movies();
        $movie->name        = $name;
        $movie->director    = $director;
        $movie->release_year  = $year;

        if($movie->create()){
            $msg = 'The movie was added successfully!';
            echo json_encode(array('success' => true, "msg"=> $msg ));
            exit();
        }else{
            $msg = 'The movie add not successfully!';
            echo json_encode(array('success' => false, "msg"=> $msg ));
            exit();
        }

    }

    public function editmovieAction(){
        $data = $this->request->getJsonRawBody();
        $id = $data->id;
        $name = $data->name;
        $director = $data->director;
        $year = $data->year;
        $msg = '';

        $movie = Movies::findFirst("id=$id");
        $movie->name        = $name;
        $movie->director    = $director;
        $movie->release_year  = $year;


        if($movie->save()){
            $msg = 'The movie was edited successfully!';
            echo json_encode(array('success' => true, "msg"=> $msg ));
            exit();
        }else{
            $msg = 'The movie edit not successfully!';
            echo json_encode(array('success' => false, "msg"=> $msg ));
            exit();
        }

    }

    public function deletemovieAction(){
        $data = $this->request->getJsonRawBody();
        $id = $data->id;
        $msg = '';

        $movie = Movies::findFirst("id= $id");

        if ($movie !== false) {
            if ($movie->delete() === false) {
                $msg = "Sorry, we can't delete the movie right now: <br>";

                $msg = $msg . $movie->getMessages();

                foreach ($messages as $message) {
                    $msg = $msg .$message .'<br>';
                }
                echo json_encode(array('success' => false, "msg"=> $msg ));
                exit();
            } else {
                $msg = 'The movie was deleted successfully!';
                echo json_encode(array('success' => true, "msg"=> $msg ));
                exit();
            }
        }
      
        
    }
   


}


?>