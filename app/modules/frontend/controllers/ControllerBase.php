<?php
namespace Frontend\Controllers;

use Phalcon\Mvc\Controller;
use Frontend\Models\Users;

class ControllerBase extends Controller
{
    public function initialize()
    {
       
        $this->view->titlepage = 'My Movies Frontend';
        $this->view->server_mode = SERVER_MODE;
        $this->view->static = $this->config->get("static")["domain"];
        $this->view->assets_domain = $this->config->get("application")["assetsDomain"];
        
    }
}
?>