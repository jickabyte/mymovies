<?php
// phpinfo();

//error_reporting(E_ALL);
error_reporting(E_ALL);

use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;

// ==> define server_mode
// define('PRODUCTION', 'production');
// define('DEV', 'dev');
define('LOCAL', 'local');
define('APP_PATH', realpath('..') . '/');
define('SERVER_MODE', LOCAL);
try{

    /**
     * Load application services.
     */
    require APP_PATH . 'app/config/services.php';

    /**
     * Auto-loader configuration.
     */
    $modules = require(APP_PATH . 'app/config/modules.php');

    $application = new Application($di);

    $application->registerModules($modules);

    echo $application->handle()->getContent();
} catch (Exception $e) {

    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
    
}
